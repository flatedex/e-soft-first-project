const gameField = [
    ['x', 'o', null],
    ['x', null, 'o'],
    ['x', 'o', 'o'],
];

function calculateWinner(gameField) {
    const gameFieldFlattened = gameField.flat();

    const lines = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
    ];

    const winnersSet = new Set();

    for (let i = 0; i < lines.length; i++) {
        const [a, b, c] = lines[i];
        if (
            gameFieldFlattened[a] &&
            gameFieldFlattened[a] === gameFieldFlattened[b] &&
            gameFieldFlattened[a] === gameFieldFlattened[c]
        ) {
            winnersSet.add(gameFieldFlattened[a]);
        }
    }

    if (winnersSet.size === 0 || winnersSet.size === 2) {
        return 'Ничья';
    } else if (winnersSet.size === 1) {
        return winnersSet.has('x') ? 'Крестики победили' : 'Нолики победили';
    }
}

alert(calculateWinner(gameField));
